/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class Triangle {
    private double h , b;
    
    public Triangle(double h,double b){
        this.h = h;
        this.b = b;
    }
    public double CalArea(){
        return 0.5*h*b;
    }
    public void setH_B(double h,double b){
        if(h <= 0 || b <= 0){
            System.out.println("Error : Hight and Base must more than zero!!!");
            return;
        }
        this.h = h;
        this.b = b;
    }
    public String getH_B(){
        return "(h = "+h+", b = "+b+")";
    }
    public String toString(){
        return "Area of triangle"+this.getH_B()+"is " +this.CalArea();
    }
    
}
