/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class Rectangle {
    private double w , l;
    
    public Rectangle(double w , double l){
        this.w = w;
        this.l = l;
    }
    public double CalArea(){
        return w*l;
    }
    public void setW_L(double w,double l){
        if(w <= 0 || l <= 0){
            System.out.println("Error : Width and Lenght must more than zero!!!");
            return;
        }
           this.w = w;
           this.l = l;
    }
    public String getW_L(){
        return "(w = " + w +", l = "+ l +")";
        
    }
    public String toString(){
        return "Area of rectangle" + this.getW_L()+" is " +  this.CalArea();
    }
}
