/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class Square {
    private double s;
    
    public Square(double s){
        this.s = s;
    }
    public double CalArea(){
        return s*s;
    }
    public void setS(double s){
        if(s <= 0){
            System.out.println("Error : Side <= 0!!!");
            return;
        }
        this.s = s;
    }
    public double getS(){
        return s;
    }
    public String toString(){
        return "Area of square" + "(s = "+this.getS() + ") is " + this.CalArea();
    }
}
