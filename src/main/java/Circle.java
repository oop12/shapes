/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class Circle {
        private double r;
        public static final double  pi = 22.0/7;
        
        public Circle(double r){
            this.r = r;
        }
        public double CalArea(){
            return pi * r *r ;
        }
        public void setR(double r){
            if(r <= 0){
                System.out.println("Error : r <= 0 !!!");
                return;
            }
            this.r = r;
        }
        public double getR(){
            return r;
        }
        @Override
        public String toString(){
            return "Area of circle "+"( r = "+ this.getR() +" ) is " +  this.CalArea();
        }
        
}
